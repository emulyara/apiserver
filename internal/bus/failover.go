package bus

import (
	"log"
	"time"
)

func (i *Instance) connectionKeeper() {
	var err error
	for {
		rabbitErr := <-i.rmqConnectionClosed
		if rabbitErr != nil {
			log.Printf("RabbitMQ connection failed, reconnecting..")
			for {
				i.rmq, err = rmqConnect()
				if err != nil {
					log.Printf("Retry RabbitMQ connection after %v", err)
					time.Sleep(500 * time.Millisecond)
				} else {
					log.Printf("Reconnected to RabbitMQ successfully")
					for {
						err = i.bus.rechannel(i.rmq)
						if err != nil {
							log.Printf("Retry RabbitMQ channel after %v", err)
							time.Sleep(500 * time.Millisecond)
						} else {
							log.Printf("Bus established RabbitMQ channel")
							break
						}
					}
					break
				}
			}
		}
	}
}
