package bus

import (
	"fmt"
	"log"
	"net"
	"time"

	"github.com/streadway/amqp"
	pb "gitlab.com/emulyara/apiserver/protos"
	"google.golang.org/grpc"
)

// Instance of an event bus,
// used to send and receive events
type Instance struct {
	listener            net.Listener
	grpc                *grpc.Server
	stop                chan bool
	rmq                 *amqp.Connection
	rmqConnectionClosed chan *amqp.Error
	bus                 *eventbusServer
}

// New creates eventbus instance with
// GRPC server and a RabbitMQ connection.
// Method checks whether infrastructure is suitable
// to be used
func New() *Instance {
	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", 8080))
	if err != nil {
		log.Fatalf("Failed to listen: %v", err)
	}
	server := grpc.NewServer()

	rmq, err := rmqConnect()
	if err != nil {
		log.Fatalf("Failed to dial: %v", err)
	}

	rmqConnectionClosed := make(chan *amqp.Error)
	rmq.NotifyClose(rmqConnectionClosed)

	bus, err := serveBus(rmq)
	if err != nil {
		log.Fatalf("Failed to establish a channel: %v", err)
	}
	pb.RegisterEventBusServer(server, bus)
	inst := &Instance{
		listener:            lis,
		grpc:                server,
		rmq:                 rmq,
		rmqConnectionClosed: rmqConnectionClosed,
		bus:                 bus,
	}
	go inst.connectionKeeper()
	return inst
}

func rmqConnect() (*amqp.Connection, error) {
	rmq, err := amqp.Dial("amqp://guest:guest@localhost:5672/")
	if err != nil {
		return nil, err
	}
	return rmq, nil
}

// Loop initiates main event loop
// and returns function to close
// running process
func (i *Instance) Loop() func() {
	go func() { i.grpc.Serve(i.listener) }()
	return func() {

		grpcStopKiller := make(chan bool)
		grpcStoppedMain := make(chan bool)

		log.Printf("Stopping GRPC server gracefully")
		go func() {
			i.grpc.GracefulStop()
			grpcStopKiller <- true
			grpcStoppedMain <- true
		}()

		go func() {
			select {
			case <-time.After(10 * time.Second):
				log.Printf("GRPC was not stopped gracefully after 10 seconds, killing...")
				i.grpc.Stop()
				log.Printf("GRPC has stopped forcefully after timeout")
				grpcStoppedMain <- true
			case <-grpcStopKiller:
				log.Printf("GRPC has stopped gracefully")
			}
		}()
		<-grpcStoppedMain

		log.Printf("Closing RabbitMQ connection")
		if err := i.rmq.Close(); err != nil {
			log.Printf("Connection close failed: %v", err)
		}
	}
}
