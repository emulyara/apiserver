package bus

import (
	"fmt"

	pb "gitlab.com/emulyara/apiserver/protos"
)

func key(ev *pb.Event) (string, *pb.EventPosted) {
	var key = ""
	if ev.Source.SourceType == pb.EventSource_SERVICE {
		// <namespace>.SERVICE.<service_name>.<name>
		key = fmt.Sprintf("%s.SERVICE.%s.%s", ev.Namespace, ev.Source.SourceId, ev.Name)
	} else if ev.Source.SourceType == pb.EventSource_VIRDEV {
		//<namespace>.VDEV.<model_id>.<name>.<device_id>
		key = fmt.Sprintf("%s.VDEV.%s.%s.%s", ev.Namespace, ev.Source.ModelId, ev.Name, ev.Source.SourceId)
	} else {
		return "", &pb.EventPosted{
			Status: pb.EventPosted_INVALID,
			Code:   "unknownSourceType",
			Reason: fmt.Sprintf("Source type must be either SERVICE=%d, or VDEV=%d, but was %d",
				pb.EventSource_SERVICE, pb.EventSource_VIRDEV, ev.Source.SourceType),
		}
	}
	return key, nil
}
