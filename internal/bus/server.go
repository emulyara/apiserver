package bus

import (
	"context"
	"fmt"
	"log"
	"sync"

	"github.com/hashicorp/go-uuid"
	"github.com/streadway/amqp"
	pb "gitlab.com/emulyara/apiserver/protos"
)

var (
	busExchange        = "emulyara.eventbus"
	busTopicExchange   = "emulyara.eventbus.topic"
	busHeadersExchange = "emulyara.eventbus.headers"

	namespaceHeader  = "emulyara.namespace"
	nameHeader       = "emulyara.name"
	sourceIDHeader   = "emulyara.source.id"
	sourceTypeHeader = "emulyara.source.type"
	eventIDHeader    = "emulyara.event.id"
	modelIDHeader    = "emulyara.model.id"
)

type eventbusServer struct {
	channel *amqp.Channel
	confirm chan amqp.Confirmation
	mux     sync.Mutex
}

func (es *eventbusServer) init(rmq *amqp.Connection) error {
	channel, err := rmq.Channel()
	if err != nil {
		return err
	}
	channel.Confirm(false)

	confirm := make(chan amqp.Confirmation)
	channel.NotifyPublish(confirm)

	err = channel.ExchangeDeclare(busExchange, "fanout", true, false, false, false, nil)
	if err != nil {
		return err
	}

	err = channel.ExchangeDeclare(busTopicExchange, "topic", true, false, false, false, nil)
	if err != nil {
		return err
	}

	err = channel.ExchangeDeclare(busHeadersExchange, "headers", true, false, false, false, nil)
	if err != nil {
		return err
	}

	err = channel.ExchangeBind(busTopicExchange, "", busExchange, false, nil)
	if err != nil {
		return err
	}

	err = channel.ExchangeBind(busHeadersExchange, "", busExchange, false, nil)
	if err != nil {
		return err
	}

	es.channel = channel
	es.confirm = confirm
	return nil
}

func serveBus(rmq *amqp.Connection) (*eventbusServer, error) {
	var server = eventbusServer{}
	if err := server.init(rmq); err != nil {
		return nil, err
	}
	return &server, nil
}

func knownNamespace(ev *pb.Event) bool {
	if ev.Namespace == "emulyara" {
		return true
	}
	return false
}

func (es *eventbusServer) rechannel(rmq *amqp.Connection) error {
	es.channel.Close()
	if err := es.init(rmq); err != nil {
		return err
	}
	return nil
}

func (es *eventbusServer) PostEvent(ctx context.Context, ev *pb.Event) (*pb.EventPosted, error) {
	if !knownNamespace(ev) {
		return unknownNamespace(ev.Namespace), nil
	}

	if ev.Source == nil {
		return errorsNotSet(), nil
	}

	if ev.Content == nil {
		return contentNotSet(), nil
	}

	key, invalid := key(ev)
	if invalid != nil {
		return invalid, nil
	}

	var mime = ""
	if ev.Content.Encoding == pb.EventContent_JSON {
		mime = "application/json"
	} else if ev.Content.Encoding == pb.EventContent_PROTOBUF {
		mime = "application/protobuf"
	} else {
		return unknownEncoding(ev.Content.Encoding), nil
	}

	id, err := uuid.GenerateUUID()
	if err != nil {
		log.Printf("Failed to generate id: %v", err)
		return backendFail("cannotGenerateIdentifier", err), nil
	}

	err = es.sendEvent(ev, key, mime, id)

	if err != nil {
		log.Printf("Failed to send event: %v", err)
		return backendFail("brokerFailed", err), nil
	}

	log.Printf("Event %s/%s %s published", ev.Namespace, ev.Name, id)

	return &pb.EventPosted{
		Status: pb.EventPosted_SUCCESS,
		Id:     id,
	}, nil
}

func (es *eventbusServer) sendEvent(ev *pb.Event, key string, mime string, id string) error {
	es.mux.Lock()
	defer es.mux.Unlock()
	err := es.channel.Publish(busExchange, key, false, false, amqp.Publishing{
		ContentType: mime,
		Body:        ev.Content.Data,
		Headers: amqp.Table{
			namespaceHeader:  ev.Namespace,
			nameHeader:       ev.Name,
			sourceIDHeader:   ev.Source.SourceId,
			sourceTypeHeader: ev.Source.SourceType.String(),
			modelIDHeader:    ev.Source.ModelId,
			eventIDHeader:    id,
		},
	})
	if err != nil {
		return err
	}

	confirmation := <-es.confirm
	if !confirmation.Ack {
		return fmt.Errorf("Broker did not confirm message with tag %d", confirmation.DeliveryTag)
	}
	return nil
}

func (es *eventbusServer) Subscribe(evs *pb.EventSubscription, sub pb.EventBus_SubscribeServer) error {
	id, err := uuid.GenerateUUID()
	if err != nil {
		log.Printf("Failed to generate subscription id: %v", err)
		return err
	}

	q, err := es.channel.QueueDeclare(id, false, true, true, false, nil)
	if err != nil {
		log.Printf("Failed to declare queue %s for subscription: %v", id, err)
		return err
	}

	deliveries, err := es.channel.Consume(q.Name, id, true, true, false, false, nil)
	if err != nil {
		log.Printf("Failed to declare consume queue %s for subscription: %v", id, err)
		return err
	}

	key := evs.BindingKey
	es.channel.QueueBind(q.Name, key, busTopicExchange, false, nil)
	if err != nil {
		log.Printf("Failed to bind queue %s for subscription on key %s: %v", id, key, err)
		return err
	}

	log.Printf("Running subscription %s", id)
	for {
		var delivery amqp.Delivery

		select {
		case <-sub.Context().Done():
			log.Printf("Subscription %s is done, stop processing", id)
			return nil
		case delivery = <-deliveries:
		}

		eventIDHeaderValue := delivery.Headers[eventIDHeader]

		//TODO: reestablish subscription without terminating GRPC
		if eventIDHeaderValue == nil {
			log.Printf("Event id header is not set, assume that subscription is over, because RabbitMQ channel is closing")
			return nil
		}

		if sub.Context().Err() != nil {
			log.Printf("Subscription %s seems to be terminated, stop processing", id)
			return nil
		}

		eventID := eventIDHeaderValue.(string)
		sourceTypeString := delivery.Headers[sourceTypeHeader].(string)
		sourceTypeValue := pb.EventSource_SourceType(pb.EventSource_SourceType_value[sourceTypeString])
		var enc pb.EventContent_ContentEncodingType
		if delivery.ContentType == "application/json" {
			enc = pb.EventContent_JSON
		} else if delivery.ContentType == "application/protobuf" {
			enc = pb.EventContent_PROTOBUF
		} else {
			log.Printf("Unknown content type: %s, skip message", delivery.ContentType)
			continue
		}

		event := pb.Event{
			Name:      delivery.Headers[nameHeader].(string),
			Namespace: delivery.Headers[namespaceHeader].(string),
			Source: &pb.EventSource{
				SourceType: sourceTypeValue,
				SourceId:   delivery.Headers[sourceIDHeader].(string),
				ModelId:    delivery.Headers[modelIDHeader].(string),
			},
			Content: &pb.EventContent{
				Data:     delivery.Body,
				Encoding: enc,
			},
		}

		err := sub.Send(&event)
		if err != nil {
			log.Printf("Cannot reply to subscription stream with event %s: %v", eventID, err)
		}
	}
}
