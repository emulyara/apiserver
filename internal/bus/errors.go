package bus

import (
	"fmt"

	pb "gitlab.com/emulyara/apiserver/protos"
)

func errorsNotSet() *pb.EventPosted {
	return &pb.EventPosted{
		Status: pb.EventPosted_INVALID,
		Code:   "sourceNotSet",
		Reason: fmt.Sprintf("Source must be specified"),
	}
}

func contentNotSet() *pb.EventPosted {
	return &pb.EventPosted{
		Status: pb.EventPosted_INVALID,
		Code:   "contentNotSet",
		Reason: fmt.Sprintf("Content must be specified"),
	}
}

func unknownNamespace(namespace string) *pb.EventPosted {
	return &pb.EventPosted{
		Status: pb.EventPosted_INVALID,
		Code:   "unknownNamespace",
		Reason: fmt.Sprintf("Namespace %s is unknown", namespace),
	}
}

func unknownEncoding(encoding pb.EventContent_ContentEncodingType) *pb.EventPosted {
	return &pb.EventPosted{
		Status: pb.EventPosted_INVALID,
		Code:   "unknownContentEncoding",
		Reason: fmt.Sprintf("Content encoding must be either JSON=%d, or PROTOBUF=%d, but was %d",
			pb.EventContent_JSON, pb.EventContent_PROTOBUF, encoding),
	}
}

func backendFail(code string, err error) *pb.EventPosted {
	return &pb.EventPosted{
		Status: pb.EventPosted_BACKEND_FAIL,
		Code:   code,
		Reason: fmt.Sprintf("%v", err),
	}
}
