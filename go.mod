module gitlab.com/emulyara/apiserver

go 1.14

require (
	github.com/golang/protobuf v1.3.3
	github.com/hashicorp/go-uuid v1.0.2
	github.com/streadway/amqp v1.0.0
	google.golang.org/grpc v1.30.0
)
