package main

import (
	"log"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/emulyara/apiserver/internal/bus"
)

func main() {
	sigs := make(chan os.Signal, 1)
	done := make(chan bool, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

	eventbus := bus.New()

	stopTheBus := eventbus.Loop()
	defer stopTheBus()
	//   <>     /--------
	//  |  |/  |         |
	go func() { //////////
		sig := <-sigs
		log.Printf("Stopping by sig %v", sig)
		done <- true
	}()

	log.Println("Event bus loop is running")
	<-done
	log.Println("Exiting...")
}
