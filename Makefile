all: gen build

gen: 
	protoc -I protos --go_out=plugins=grpc:protos --go_opt=paths=source_relative protos/api.proto

run: gen 
	go build -o emulapi cmd/apiserver/emulyara-apiserver.go && ./emulapi
	
post:
	grpcurl -plaintext -import-path ./protos -proto api.proto \
	  -d '{ "content": {}, "source": {"sourceType": "SERVICE", "sourceId": "test"}, "namespace": "emulyara", "name": "gogogo"}' \
	  localhost:8080 \
	  emulyara.EventBus/PostEvent
	
subscribe:
	grpcurl -plaintext -import-path ./protos -proto api.proto \
	  -d '{ "bindingKey": "*.*.*.*" }' \
	  localhost:8080 \
	  emulyara.EventBus/Subscribe

example-client:
	go run examples/client.go