# Emulyara API Server - API for IoT emulator system

`Under development notice`: project is not fully implemented yet

## About

Emulyara is a project to connect cloud developers with IoT device 
vendors. 

The goal is to have a platform, where backend developers 
could easily test their services against virtual devices 
running as close as possible emulations of real things, provided
by IoT device manufacturers.

One of important criterias is to have as much devices supported as 
possible, which could be only reached, if writing virtual device 
emulators would be easy, that's why API Server provides generic 
GRPC API to access Emulyara Event Bus. With GRPC client generators
anyone could write service running a certain virtual device 
emulation using language, which is the most convenient for 
device developers.

