package main

import (
	"context"
	"log"
	"os"
	"os/signal"
	"syscall"

	pb "gitlab.com/emulyara/apiserver/protos"
	"google.golang.org/grpc"
)

func listen(client pb.EventBusClient) {
	subscription, err := client.Subscribe(context.Background(), &pb.EventSubscription{
		BindingKey: "*.*.*.*",
	})
	if err != nil {
		log.Fatal(err)
	}
	go func() {
		for {
			event, err := subscription.Recv()

			if err != nil {
				log.Fatal(err)
			}
			log.Printf("Got %s/%s", event.Namespace, event.Name)
		}
	}()
}

func main() {
	conn, err := grpc.Dial("localhost:8080", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("failed to dial: %v", err)
	}
	client := pb.NewEventBusClient(conn)
	listen(client)
	posted, err := client.PostEvent(context.Background(), &pb.Event{
		Name:      "test",
		Namespace: "emulyara",
		Source:    &pb.EventSource{},
		Content: &pb.EventContent{
			Encoding: pb.EventContent_JSON,
			Data:     []byte("{}"),
		},
	})
	if err != nil {
		log.Fatalf("failed to post: %v", err)
	}
	log.Printf("%v", posted)

	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)
	<-sigs

}
